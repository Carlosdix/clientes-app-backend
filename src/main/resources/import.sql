/*
SELECT table_name --seleccionamos solo la columna del nombre de la tabla
FROM information_schema.tables --seleccionamos la información del esquema
WHERE table_schema='public' --las tablas se encuentran en el esquema publico
  AND table_type='BASE TABLE';
  */

 */
/*
1085335964,ALVAREZ ERASO,2018-02-20,matalvarez@umariana.edu.co,MATEO FABIAN
1082749257,DIAZ BASANTE,2018-02-20,carlodiaz@umariana.edu.co,CARLOS ERNESTO
1010094599,CHANA CHAMORRO,2018-02-20,nchana@umariana.edu.co,NICOLAS SANTIAGO
1233190307,BOTINA HERRERA,2018-02-20,andrbotina@umariana.edu.co,ANDRES MAURICIO
1233189273,GUAQUEZ YAMA,2018-02-20,joguaquez@umariana.edu.co,JOHN JAIRO
1085297757,BASTIDAS RODRIGUEZ,2018-02-20,danielbastidas@umariana.edu.co,DANIEL ESTEBAN
1085343497,BENAVIDES BETANCOURT,2018-02-20,luiscbenavides@umariana.edu.co,LUIS CARLOS
1085332017,JOJOA PRADO,2018-02-20,vijojoa@umariana.edu.co,BRANDON ANDRES
1004572802,GUEVARA ROMO,2018-02-20,naguevara@umariana.edu.co,NATALIA ANDREA
1124865024,JIMENEZ CHANCHI,2018-02-20,bjimenez@umariana.edu.co,BRANDON ANDRES
1004572572,GUEVARA ROMO,2018-02-20,alguevara@umariana.edu.co,ALEX RUBEN
1004214831,NARVAEZ MUÑOZ,2018-02-20, madnarvaez@umariana.edu.co, MADELINE DANIELA
1086106270,NARVAEZ VELASCO,2018-02-20, mignarvaez@umariana.edu.co,MIGUEL ANGEL
1122143677,ORDOÑEZ SATIZABAL,2018-02-20, favordonez@umariana.edu.co,FAVER ANDREI
1085329828,ORTIZ JEREZ,2018-02-20, camortiz@umariana.edu.co,CAMILO ERNESTO
1004189851,PANTOJA PANTOJA,2018-02-20,oscarapantoja@umariana.edu.co,OSCAR ALEJANDRO
1087648179,PIZARRO GOMEZ,2018-02-20, jpizarro@umariana.edu.co,LI DAMIAN
1088651748,PUETATE REINA,2018-02-20, jpuetate@umariana.edu.co,JONATHAN ANDREY
1004232150,RODRIGUEZ BASTIDAS,2018-02-20, jhorrodriguez@umariana.edu.co,JHORLEN ANDRES
1193273265,SANTACRUZ CARVAJAL,2018-02-20, davsantacruz@umariana.edu.co,DAVID ALEJANDRO
1085343843,SOLARTE PATIÑO,2018-02-20, camsolarte@umariana.edu.co,CAMILO HERNAN
17,Torvalds,2021-01-09,linus.torvaldsr@linuxmail.com,Linus
13,Doe,2021-01-09,jade.doegmail.com,Jade
8,Johnson,2021-01-09,ralp.jhonson@gmail.com,Ralph
2,Guzman,2021-01-09,profesor@bolsadeideas.com,Andres
15,Hills,2021-01-09,peter.hills@gmail.com,Peter
1,Díaz,2021-01-09,carlodiaz@umariana.edu.co,Carlos
6,Gamma,2021-01-09,erich.gamma@gmail.com,Erich
11,Lee,2021-01-09,magna.lee@gmail.com,Magna
5,Lerdorf,2021-01-09,rasmus.lerdorf@gmail.com,Rasmus
10,Gosling,2021-01-09,james.golsing@gmail.com,Dr. James
3,Doe,2021-01-09,jhon.doe@gmail.com,Mr. Jhon
9,Vlissides,2021-01-09,jhon.vlissides@gmail.com,Jhon
7,Helm,2021-01-09,richard.helm@gmail.com,Richard
4,Torvalds,2021-01-09,linus.torvaldsr@gmail.com,Linus
12,Roe,2021-01-09,tornado.roe@gmail.com,Tornado
14,Baggins,2021-01-09,bilbo.baggins@gmail.com,Bilbo
*/

/*-----------------------------------------------------------------------------*/

INSERT INTO regiones (nombre) VALUES ('NorteAmerica');
INSERT INTO regiones (nombre) VALUES ('CentroAmerica');
INSERT INTO regiones (nombre) VALUES ('Sudamerica');
INSERT INTO regiones (nombre) VALUES ('Europa');
INSERT INTO regiones (nombre) VALUES ('Africa');
INSERT INTO regiones (nombre) VALUES ('Oceania');
INSERT INTO regiones (nombre) VALUES ('Asia');
INSERT INTO regiones (nombre) VALUES ('Antartida');

INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Díaz','carlodiaz@umariana.edu.co','2020-12-08','Carlos','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (3,'Guzman','profesor@bolsadeideas.com','2018-01-01','Andres','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (4,'Does','jhon.doe@gmail.com','2018-01-02','Mr. Jhon','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (3,'Torvalds','linus.torvaldsr@gmail.com','2018-01-03','Linus','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Lerdorf','rasmus.lerdorf@gmail.com','2018-01-04','Rasmus','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Gamma','erich.gamma@gmail.com','2018-02-01','Erich','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Helm','richard.helm@gmail.com','2018-02-10','Richard','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Johnson','ralp.jhonson@gmail.com','2018-02-18','Ralph','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (2,'Vlissides','jhon.vlissides@gmail.com','2020-02-29','Jhon','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (2,'Gosling','james.golsing@gmail.com','2018-03-03','Dr. James','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (5,'Lee','magna.lee@gmail.com','2018-03-04','Magna','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (5,'Roe','tornado.roe@gmail.com','2018-03-05','Tornado','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (6,'Doe','jade.doegmail.com','2018-03-06','Jade','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Baggins','bilbo.baggins@gmail.com','2020-12-09','Bilbo','');
INSERT INTO clientes ( region_id,apellido,email,create_at,nombre,foto ) VALUES (1,'Hills','peter.hills@gmail.com','2021-01-04','Peter','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Faver','Ordoñez','2021-01-04','favor@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Oscar','Pantoja','2021-02-04','oscar@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Luis','Benavidez','2021-02-04','luisbena@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Camilo','Solarte','2021-02-04','camsolarte@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Andres','Botina','2021-02-04','andrbotina@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Michael','Olaya','2021-02-04','molaya@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Daniel','Bastidas','2021-02-04','danielbastidas@umariana.edu.co'v);
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Madeline','Narvaez','2021-02-04','madnarvaez@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Natalia','Guevara','2021-02-04','naguevara@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Jonathan','Puetate','2021-02-04','jpuetate@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Miguel','Narvaez','2021-02-04','mignarvaez@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'John','Guaquez','2021-02-04','joguaquez@umariana.edu.co','');
INSERT INTO clientes ( region_id,nombre,apellido,create_at,email,foto ) VALUES (3,'Jhorlen','Rodriguez','2021-02-04','jhorrodriguez@umariana.edu.co','');


INSERT INTO usuarios (nombre,password,habilitado) VALUES ('CarlosDiz','$2a$10$QAeqeyp1mgwJBEWt44wNW.tW7ijl1vA8SVvuzL5Z5VmET/Yw2Ab8O',true);
INSERT INTO usuarios (nombre,password,habilitado) VALUES ('admin','$2a$10$F5YXyB5bKZ.zKTYR5InTjOLsoirS/Me9/zcoRrpnZbnwEfnz2U8li',true);

INSERT INTO roles (nombre) VALUES ('ROLE_USER');
INSERT INTO roles (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO usuarios_roleados (usuario_id, rol_id) VALUES (1,1);
INSERT INTO usuarios_roleados (usuario_id, rol_id) VALUES (2,2);
INSERT INTO usuarios_roleados (usuario_id, rol_id) VALUES (2,1);

/*-----------------------------------------------------------------------------*/


SELECT * FROM clientes;