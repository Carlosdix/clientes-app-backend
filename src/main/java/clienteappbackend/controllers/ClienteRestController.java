package clienteappbackend.controllers;

import clienteappbackend.models.entity.Cliente;
import clienteappbackend.models.entity.Region;
import clienteappbackend.models.services.IClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"http://localhost:4200","*"})
@RestController
@RequestMapping("/api/clientes")
public class ClienteRestController
{
    private final static File SIN_FOTO = new File("src/main/resources/static/images/sin-foto.png");

    private final static HttpHeaders HEADERS = new HttpHeaders();

    @Autowired
    private IClienteService service;

    private final Logger logger = LoggerFactory.getLogger(ClienteRestController.class);

    public ClienteRestController() throws MalformedURLException {
    }

    @GetMapping({"","/","/list"})
    public List<Cliente> list()
    {
        return service.findAll();
    }

    @GetMapping("/page/{page}")
    public Page<Cliente> paginate(@PathVariable Integer page)
    {
        return service.findAll(PageRequest.of(page,4));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> show(@PathVariable Long id)
    {
        final Map<String,Object> response =new HashMap<>();
        try {
            final Cliente cliente = service.findById( id );
            if (cliente==null){
                response.put("Mensaje","El cliente con el id: ".concat(id.toString()).concat(" no existe!"));
                return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<Cliente>(cliente,HttpStatus.OK);
        }
        catch (DataAccessException e){
            response.put("Mensaje","No se ha logrado realizar la consulta en la base de datos");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage() ));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    public ResponseEntity<?> create(@Valid @RequestBody Cliente pCliente, BindingResult pResult)
    {
        final Map<String,Object> response =new HashMap<>();
        if (pResult.hasErrors()){
            final List<String> errores = pResult.getFieldErrors().stream().map(
                    e -> "El campo '"+e.getField()+"' "+e.getDefaultMessage()
            ).collect(Collectors.toList());
            response.put("errores",errores);
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }
        try {
            final Cliente cliente = service.save( pCliente );
            if (cliente==null){
                response.put("Mensaje","El cliente no se ha podido registrar, verique los datos deben ser unicos!");
                return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
            }
            response.put("Mensaje","El cliente se ha creado con exito");
            response.put("Cliente",cliente);
            return new ResponseEntity<>(response,HttpStatus.CREATED);
        }
        catch (DataAccessException e){
            response.put("Mensaje","No se ha logrado realizar la insercion en la base de datos");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage() ));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Cliente pCliente,BindingResult pResult,@PathVariable Long id )
    {
        final Map<String,Object> response =new HashMap<>();
        if (pResult.hasErrors()){
            final List<String> errores = pResult.getFieldErrors().stream().map(
                    e -> "El campo '"+e.getField()+"' "+e.getDefaultMessage()
            ).collect(Collectors.toList());
            response.put("errores",errores);
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }
        try{
            final Cliente cliente = service.findById(id);
            if (cliente==null){
                response.put("Mensaje","El cliente con el id: ".concat(id.toString()).concat(" no existe!")
                        .concat(",no se puede editar"));
                return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
            }
            cliente.setNombre( pCliente.getNombre() );
            cliente.setApellido( pCliente.getApellido() );
            cliente.setEmail( pCliente.getEmail() );
            cliente.setCreateAt( pCliente.getCreateAt() );
            cliente.setRegion( pCliente.getRegion() );
            response.put("Cliente",service.save( cliente ));
            response.put("Mensaje","El cliente ha sido actualizado");
            return new ResponseEntity<>(response,HttpStatus.CREATED);
        }
        catch (DataAccessException e){
            response.put("Mensaje","No se ha logrado realizar la actualizacion de datos del cleitne");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage() ));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Map<String,Object>> delete(@PathVariable Long id)
    {
        final Map<String,Object> response =new HashMap<>();
        try {
            final File carpeta = new File("uploads/"+id+"/");
            final File archivos[] = carpeta.listFiles();
            if (archivos != null){
                for (File archivo : archivos) {
                    archivo.delete();
                }
                carpeta.delete();
                service.delete(id);
            }
            response.put("Mensaje","Cliente eliminado con exito👍!");

            return new ResponseEntity<>(response,HttpStatus.OK);
        }
        catch (DataAccessException e){
            response.put("Mensaje","No se ha logrado eliminar el cliente".concat(id.toString()).concat(" de la base de datos"));
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage() ));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/upload/")
    public ResponseEntity<Map<String,Object>> upload(@RequestParam("archivo")MultipartFile multipartFile, @RequestParam("id")Long id){
        final Map<String,Object> response =new HashMap<>();
        final Cliente cliente = service.findById(id);
        if (cliente==null){
            response.put("Error","No se encontro el cliente con el id: '"+id+"'");
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
        if (!multipartFile.isEmpty()){
            final File carpeta = new File("uploads/"+id+"/");
            carpeta.mkdirs();
            final File archivos[] = carpeta.listFiles();
            for (File archivo : archivos) {
                archivo.delete();
            }
            final String nombreArchivo = multipartFile.getOriginalFilename().replace(" ","");
            final Path ruta = Paths.get(carpeta.getPath()).resolve(nombreArchivo).toAbsolutePath();
            logger.info(ruta.toString());
            try {
                Files.copy(multipartFile.getInputStream(),ruta);
            } catch (IOException e) {
                response.put("Mensaje","No se ha logrado subir la imagen a la base de datos");
                response.put("Error", e.getMessage().concat(": ").concat(e.getCause().getMessage() ));
                return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
            }
            cliente.setFoto( nombreArchivo );
            service.save(cliente);

            response.put("Mensaje","El cliente ha sido actualizado, imagen subida correctamente!");
            response.put("Cliente",cliente);
            return new ResponseEntity<>(response,HttpStatus.CREATED);
        }
        response.put("Error","Verifique el archivo no este vacio!");
        return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/upload/img/{id}")
    public ResponseEntity<?>verFoto(@PathVariable Long id){
        final Map<String,Object> response =new HashMap<>();
        final Cliente cliente = service.findById(id);
        if (cliente ==null){
            response.put("Mensaje","Cliente con el id: '"+id+"' no existe!");
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
        if (cliente.getFoto().equals("")) {
            response.put("Mensaje","Cliente sin foto");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        try {
            final File archivo = new File("uploads/"+id+"/"+cliente.getFoto());
            logger.info(archivo.getPath());
            final Resource recurso = new UrlResource(archivo.toURI());
            if (!recurso.exists() && !recurso.isReadable()){
                final File archivoAuxiliar = new File("src/main/resources/static/images/sin-foto.png");
                final HttpHeaders headers = new HttpHeaders();
                final Resource recursoAuxliar = new UrlResource(archivoAuxiliar.toURI());
                headers.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+recursoAuxliar.getFilename()+"\"");
                logger.error("Error al cargar la imagen de: "+id+", archivo no encontrado!");
                return new ResponseEntity<>(recursoAuxliar,headers,HttpStatus.OK);
            }
            final HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+recurso.getFilename()+"\"");
            return new ResponseEntity<>(recurso,headers,HttpStatus.OK);
        } catch (MalformedURLException e) {
            final File archivoAuxiliar = new File("src/main/resources/static/images/sin-foto.png");
            try {
                final Resource recursoAuxliar = new UrlResource(SIN_FOTO.toURI());
                HEADERS.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+recursoAuxliar.getFilename()+"\"");
                logger.error("Error al cargar la imagen de: "+id+", archivo no encontrado!");
                return new ResponseEntity<>(recursoAuxliar,HEADERS,HttpStatus.OK);
            }
            catch (MalformedURLException ee){
                response.put("Mensaje","La foto 'sin-foto.png' no es accesible");
                response.put("Error",ee.getMessage());
                return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }

    @GetMapping("/regiones")
    public List<Region> listarRegionres()
    {
        return service.findAllRegiones();
    }
}
