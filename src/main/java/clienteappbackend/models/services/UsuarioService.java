package clienteappbackend.models.services;

import clienteappbackend.models.dao.IUsuarioDao;
import clienteappbackend.models.entity.Usuario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements IUsuarioService,UserDetailsService {

    private Logger log = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private IUsuarioDao dao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String nombre) throws UsernameNotFoundException
    {
        final Usuario usuario = dao.findByNombre(nombre);
        if (usuario == null){
            log.error("Error en el login: No existe el usuario '"+nombre+"' en el sistema");
            throw new UsernameNotFoundException("Error en el login: No existe el usuario '"+nombre+"' en el sistema");
        }
        final List<GrantedAuthority>authorities =  usuario.getRoles().stream()
                .map(rol -> new SimpleGrantedAuthority(rol.getNombre()))
                .peek(simpleGrantedAuthority -> log.info("Role: "+simpleGrantedAuthority.getAuthority()) )
                .collect(Collectors.toList());
        return new User(usuario.getNombre(),usuario.getPassword(),usuario.isHabilitado(),true,true,true,authorities);
    }

    @Override
    public Usuario findByNombre(String nombre) {
        return dao.findByNombre(nombre);
    }
}
