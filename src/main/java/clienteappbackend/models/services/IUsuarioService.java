package clienteappbackend.models.services;

import clienteappbackend.models.entity.Usuario;

public interface IUsuarioService
{
    Usuario findByNombre(String nombre);
}
