package clienteappbackend.models.services;

import clienteappbackend.models.entity.Cliente;
import clienteappbackend.models.entity.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IClienteService
{
    List<Cliente> findAll();

    Page<Cliente> findAll(Pageable pPageable);

    Cliente   findById(Long pId );

    Cliente save(Cliente cliente);

    void delete( Long pId );

    List<Region> findAllRegiones();

}
