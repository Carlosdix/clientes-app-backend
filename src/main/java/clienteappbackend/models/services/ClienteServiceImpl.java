package clienteappbackend.models.services;

import clienteappbackend.models.dao.IClienteDao;
import clienteappbackend.models.entity.Cliente;
import clienteappbackend.models.entity.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteServiceImpl implements IClienteService
{
    @Autowired
    private IClienteDao dao;

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll()
    {
        return (List<Cliente>) dao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Cliente> findAll(Pageable pPageable) {
        return dao.findAll(pPageable);
    }

    @Override
    @Transactional(readOnly = true)

    public Cliente findById(Long pId) {
        return dao.findById(pId).orElse(null);
    }

    @Override
    @Transactional
    public Cliente save(Cliente cliente) {
        return dao.save(cliente);
    }

    @Override
    @Transactional
    public void delete(Long pId)
    {
        dao.deleteById(pId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Region> findAllRegiones() {
        return dao.findAllRegiones();
    }
}
