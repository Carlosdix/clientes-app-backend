package clienteappbackend.models.dao;

import clienteappbackend.models.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface IUsuarioDao extends CrudRepository<Usuario,Integer> {

    Usuario findByNombre(String nombre);

    @Query("SELECT u FROM Usuario u WHERE u.nombre=?1")
    Usuario findByNombre2(String nombre);
}
