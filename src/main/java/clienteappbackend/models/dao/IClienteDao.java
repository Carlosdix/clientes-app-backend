package clienteappbackend.models.dao;

import clienteappbackend.models.entity.Cliente;
import clienteappbackend.models.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IClienteDao extends JpaRepository<Cliente, Long>
{
    @Query("from Region")
    List<Region> findAllRegiones();
}
