package clienteappbackend.auth;

public class JwtConfig {
    public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";

    public final static String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEAuVhrSXPmyuQR/rFRae9dSdUFPwQ5G00B02bt4sANwTk2Xt26\n" +
            "yQDzsbH1L47cl0XLWEXJkceBvlZa22Jou+AQTSuFzyGsuMCNMXEHIxW5+qMLG5Fh\n" +
            "zERhnsVZUxrTuM5ZiQXSygQ0yIi6xqQI6ysUwXyifcaCIjl6FwzKcy/9NzYTH9HV\n" +
            "rk8gaL1qnbFFVv8vAk3UkUWXqUEdJRlVaQQ3ikX19rGjtBCRLpD8NaJz9NygMq+p\n" +
            "ImSH3biLuG1yt77pSbsZFKxtJkLTHm41FKvTi5VMcv5zb3Qo0Q7zLhfM8rtzKmqh\n" +
            "fjrA1t0JNAExwU/cSflaY+mAIWfk+WAdqLLZPwIDAQABAoIBAFsp9WNkobFL6tn2\n" +
            "h4/uj1kgcPsk7Z3TC8Haj8mHI3zV0jtpHlV9pV/So0GoKDIzvygE83dDxTpEqV3d\n" +
            "EpzOQdZjQOa3rogVFZ2q9ZK5PgfcqDsAEDVzsPxyH1R0Do2I2Zs/BQYeeYPlVNEd\n" +
            "n9FkGEpvLlFlEV8aHEY4IV/0y7yagxZ5ze2TB6YV61V7x5krN1AreOtOUEvFRdDs\n" +
            "kwMyO2Bn69r/LBom0GrUHt2r7SA/nWXZTcf93PFQ5nMpPUc94TxrcGUT4IAuW2hT\n" +
            "0lrtBgsWxRcdZ2oZtl75kZ0dArdQ/snmfEnqX8hRGCECnm4UPumL9DNVEAShbFbK\n" +
            "ewWL9zECgYEA2sxka2zCXAfxkPDYPtoSjktjNSIwUkizlerPJ8/5Kr0SPCOnnhlH\n" +
            "0X4PA0d5u3fPtywXqPvhvOKuzDk+Db2Rou3I9N8EV9avCcKHkHM9HVJqp7NyA3MN\n" +
            "qB7yEeulwHwf9rWHvCUhIOa308lePC31U88n5jlRThp2GnsFS3+NzlkCgYEA2Nvs\n" +
            "AJCwZEZivSU+IVTN7/QZ09GpSongd2oMQ+pve95q0LJyP2jW8alO6vJYkiUI7STQ\n" +
            "b+U7rwIz5XkzF36SQebI7PSKZIVLwkDx8E0IgrZysmy6lRPX3c51ByLNB6fvd/RZ\n" +
            "GzSQKB3RpI0zZB5DnQ7scQ0MpDcLMlhfBACiYVcCgYEAolfzzq11DPor6CJ8IS6H\n" +
            "hCM1qjAC8JrW/U6rhFvRAkAlro+6CBmz6yIAm6tGDK5aZgwGRjEWjqMXlZqHi4Rz\n" +
            "8hNC/0YWn+JAIBrBoZGP8Bi94E7KMvsEUBEeBeP2KXsv8EtDOnvGN/ZYwYDxNe/X\n" +
            "zuavMSioRi5mIUHUbO9o1YkCgYEAoMJPbo/UI1HGSp88MqtSSupy5wKK9BXHz95O\n" +
            "n15ALNxR5MoURYtrpBQ1y5fCLvzDZEEhP4H4v6DrZdZ2GJmcxLvYAUi6+w+RfLx2\n" +
            "XgRNJdEor7pIitD9+pDdUl/rDBzjOdJq5+8MWqDYU5rVGQgSaXCxuJDK6UVgyH6Y\n" +
            "WTg7J9sCgYAftT8UVKlB0HfNgCx1y8FhPCfnX3DrFZoxjwsF5qzuYp/6UtfGN2Z+\n" +
            "92gEF2aggI5yM3a19566zdkZcjchMqU0OTjos3s3btoIi9o6hqUkVnLUpOJ64FLw\n" +
            "Oag5vVa5Ew5Sc/tGel3fJJPe8sfpcYsR7Z51oeBGjQC3FHB060H//g==\n" +
            "-----END RSA PRIVATE KEY-----";

    public final static String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuVhrSXPmyuQR/rFRae9d\n" +
            "SdUFPwQ5G00B02bt4sANwTk2Xt26yQDzsbH1L47cl0XLWEXJkceBvlZa22Jou+AQ\n" +
            "TSuFzyGsuMCNMXEHIxW5+qMLG5FhzERhnsVZUxrTuM5ZiQXSygQ0yIi6xqQI6ysU\n" +
            "wXyifcaCIjl6FwzKcy/9NzYTH9HVrk8gaL1qnbFFVv8vAk3UkUWXqUEdJRlVaQQ3\n" +
            "ikX19rGjtBCRLpD8NaJz9NygMq+pImSH3biLuG1yt77pSbsZFKxtJkLTHm41FKvT\n" +
            "i5VMcv5zb3Qo0Q7zLhfM8rtzKmqhfjrA1t0JNAExwU/cSflaY+mAIWfk+WAdqLLZ\n" +
            "PwIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
