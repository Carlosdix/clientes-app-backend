package clienteappbackend.auth;

import clienteappbackend.models.entity.Usuario;
import clienteappbackend.models.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InfoToken implements TokenEnhancer
{
    @Autowired
    private IUsuarioService service;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        final Usuario usuario = service.findByNombre(oAuth2Authentication.getName());
        final Map<String,Object> info = new HashMap<>();

        info.put("info_adicional","Hola que tal!: ".concat(oAuth2Authentication.getName()));
        info.put("usuario",usuario.getId()+": "+usuario.getNombre());
        ((DefaultOAuth2AccessToken)oAuth2AccessToken).setAdditionalInformation(info);
        return oAuth2AccessToken;
    }
}
